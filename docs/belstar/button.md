<h1>Button</h1>
<div style="width:100%">
<h3>Default</h3>
<h5>按鈕預設樣式，重點加重框陰影。</h5>
<v-btn style="background:#2386c9;color:#fff;margin-bottom:10px;margin-left:0"> Button </v-btn>
</div>
<img :src="$withBase('/images/button size.png')" alt="foo" height="150">
<div style="min-width:800px">
<v-expansion-panel >
<v-expansion-panel-content v-for="(item,i) in 1":key="i" style="background:#ebebeb" >
<template v-slot:header>
<div>HTML</div>
</template>
<v-card>
<div>

```html
<v-btn style="background:#2386c9;color:#fff;margin-bottom:10px;margin-left:0">Button </v-btn>
```

</div>
</v-card>
</v-expansion-panel-content>
</v-expansion-panel>
</div>

<h3>Flat</h3>
<h5>按鈕扁平樣式，沒有陰影。</h5>
<v-btn style="background:#2386c9;color:#fff;box-shadow:none;margin-bottom:10px;margin-left:0"> Button </v-btn>
</div>
<div style="min-width:800px">
<v-expansion-panel>
<v-expansion-panel-content v-for="(item,i) in 1":key="i" style="background:#ebebeb">
<template v-slot:header>
<div>HTML</div>
</template>
<v-card>
<div>

```html
 <v-btn style="background:#2386c9;color:#fff;box-shadow:none;margin-bottom:10px;margin-left:0"> Button </v-btn>
```

</div>
</v-card>
</v-expansion-panel-content>
</v-expansion-panel>
</div>

<h3>Disabled</h3>
<h5>按鈕禁止點擊樣式。</h5>
<v-btn disabled style="margin-bottom:10px;margin-left:0"> Button </v-btn>
</div>
<div style="min-width:800px">
<v-expansion-panel>
<v-expansion-panel-content v-for="(item,i) in 1":key="i" style="background:#ebebeb">
<template v-slot:header>
<div>HTML</div>
</template>
<v-card>
<div>

```html
 <v-btn disabled style="margin-bottom:10px;margin-left:0"> Button </v-btn>
```

</div>
</v-card>
</v-expansion-panel-content>
</v-expansion-panel>
</div>

<h3>Outline</h3>
<h5>按鈕邊框樣式，沒有陰影。</h5>
<v-btn outline style="color:#2386c9;margin-bottom:10px;margin-left:0"> Button </v-btn>
</div>
<div style="min-width:800px">
<v-expansion-panel>
<v-expansion-panel-content v-for="(item,i) in 1":key="i" style="background:#ebebeb">
<template v-slot:header>
<div>HTML</div>
</template>
<v-card>
<div>

```html
 <v-btn disabled style="color:#2386c9;margin-bottom:10px;margin-left:0"> Button </v-btn>
```

</div>
</v-card>
</v-expansion-panel-content>
</v-expansion-panel>
</div>

<h3>Round</h3>
<h5>圓角按鈕行為與常規按鈕相同，具有圓角邊緣。</h5>
<v-btn round style="background:#2386c9;color:#fff;margin-bottom:10px;margin-left:0"> Button </v-btn>
</div>
<br>
<img :src="$withBase('/images/buttonsizeround.png')" alt="foo" height="140">
<div style="min-width:800px">
<v-expansion-panel>
<v-expansion-panel-content v-for="(item,i) in 1":key="i" style="background:#ebebeb">
<template v-slot:header>
<div>HTML</div>
</template>
<v-card>
<div>

```html
 <v-btn disabled style="color:#2386c9;margin-bottom:10px;margin-left:0"> Button </v-btn>
```

</div>
</v-card>
</v-expansion-panel-content>
</v-expansion-panel>
</div>

<v-btn round style="background:#2386c9;color:#fff;box-shadow:none;margin-bottom:10px;margin-left:0"> Button </v-btn>
</div>
<div style="min-width:800px">
<v-expansion-panel>
<v-expansion-panel-content v-for="(item,i) in 1":key="i" style="background:#ebebeb">
<template v-slot:header>
<div>HTML</div>
</template>
<v-card>
<div>

```html
 <v-btn disabled style="background:#2386c9;color:#fff;box-shadow:none;margin-bottom:10px;margin-left:0"> Button </v-btn>
```

</div>
</v-card>
</v-expansion-panel-content>
</v-expansion-panel>
</div>

<v-btn round outline style="color:#2386c9;box-shadow:none;margin-bottom:10px;margin-left:0"> Button </v-btn>
</div>
<div style="min-width:800px">
<v-expansion-panel>
<v-expansion-panel-content v-for="(item,i) in 1":key="i" style="background:#ebebeb">
<template v-slot:header>
<div>HTML</div>
</template>
<v-card>
<div>

```html
 <v-btn disabled style="background:#2386c9;color:#fff;box-shadow:none;margin-bottom:10px;margin-left:0"> Button </v-btn>
```

</div>
</v-card>
</v-expansion-panel-content>
</v-expansion-panel>
</div>

<h3>Block</h3>
<h5>可擴展至全部可用寬度的按鈕。</h5>
<v-btn block style="background:#2386c9;color:#fff;margin-bottom:10px;margin-left:0"> Button </v-btn>
</div>
<div style="min-width:800px">
<v-expansion-panel>
<v-expansion-panel-content v-for="(item,i) in 1":key="i" style="background:#ebebeb">
<template v-slot:header>
<div>HTML</div>
</template>
<v-card>
<div>

```html
 <v-btn disabled style="background:#2386c9;color:#fff;box-shadow:none;margin-bottom:10px;margin-left:0"> Button </v-btn>
```

</div>
</v-card>
</v-expansion-panel-content>
</v-expansion-panel>
</div>

<v-btn block style="background:#2386c9;color:#fff;box-shadow:none;margin-bottom:10px;margin-left:0"> Button </v-btn>
</div>
<div style="min-width:800px">
<v-expansion-panel>
<v-expansion-panel-content v-for="(item,i) in 1":key="i" style="background:#ebebeb">
<template v-slot:header>
<div>HTML</div>
</template>
<v-card>
<div>

```html
 <v-btn disabled style="background:#2386c9;color:#fff;box-shadow:none;margin-bottom:10px;margin-left:0"> Button </v-btn>
```

</div>
</v-card>
</v-expansion-panel-content>
</v-expansion-panel>
</div>





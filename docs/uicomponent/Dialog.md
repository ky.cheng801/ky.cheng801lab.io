<h1>對話框</h1>
<img :src="$withBase('/images/7.0dialogs.png')" alt="dialogs" width="1000">
<img :src="$withBase('/images/7.1alertdialog.png')" alt="alertdialog" width="1000">
<img :src="$withBase('/images/7.2confirmationdialog.png')" alt="confirmationdialog" width="1000">
<img :src="$withBase('/images/7.3inputdialog.png')" alt="inputdialog" width="1000">
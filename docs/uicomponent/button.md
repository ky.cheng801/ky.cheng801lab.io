<h1>按鈕</h1>

`將圖片放到.vuepress/public/images資料夾下，再將:src="$withBase('/images/last-logo.png')改掉`
<img class="bs-toolbar-icon" :src="$withBase('/images/last-logo.png')" />

<img :src="$withBase('/images/test.png')" alt="foo" width="1000">
<p>按鈕分為三種型態，分別用在不同情境</p>
<ol>
<li>扁平按鈕：使用在對話框或文件內</li>
<li>浮動按鈕：使用在頁面</li>
<li>FAB(浮動操作按鈕)：同樣使用在頁面，不同於浮動按鈕，FAB可連結其他操作動作，並為應用程式中的主要操作</li>
</ol>
<br>
<p>動作分為正常、滑鼠徘徊、點擊和鎖定</p>
<br>
<h2>按鈕尺寸</h2>
<img src="https://i.imgur.com/jVN1FSx.png" alt="foo" width="1000">


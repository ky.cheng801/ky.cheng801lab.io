module.exports = {
  title: 'GitLab ❤️ VuePress',
  description: 'Vue-powered static site generator running on GitLab Pages',
  base: '/',
  dest: 'public',
  head: [
    ['link', { rel: 'stylesheet', href: `https://fonts.googleapis.com/css?family=Material+Icons` }],
    ['link', { rel: 'stylesheet', href: `https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css` }]
  ],
  theme: '',
  themeConfig: {
    menus: [
      {
        text: 'Home',
        icon: 'home',
        url: '/'
      },
      {
        text: 'Layout',
        icon: 'dashboard',
        url: '/layout',
        items: [
          {
            text: 'Web layout',
            url: 'web-layout.html'
          },
          {
            text: 'Mobile layout',
            url: 'mobile-layout.html'
          }
        ]
      },
      {
        text: 'Color',
        icon: 'palette',
        url: '/color',
        items: [
          {
            text: 'The color system',
            url: 'the-color-system.html'
          }
        ]
      },
      {
        text: 'UI Components',
        icon: 'widgets',
        url: '/uicomponent',
        items: [
          {
            text:'Banner',
            url:'Banner.html'
          },
          {
            text: 'Button',
            url: 'Button.html'
          },
          {
            text:'Dialog',
            url:'Dialog.html'
          },
          {
            text:'Icon',
            url:'Icon.html'
          },
          {
            text: 'List',
            url: 'List.html'
          },
          {
            text:'Menu',
            url:'Menu.html'
          },
          {
            text:'Search Bar',
            url:'Search-Bar.html'
          },
          {
            text: 'Selection Control',
          url: 'Selection-Control.html'
        },
        {
          text:'Side Sheet',
          url:'Side-Sheet.html'
        },
        {
          text:'Slider',
          url:'Slider.html'
        },
        {
          text:'Snackbar',
          url:'Snackbar.html'
        },
        {
          text:'Stepper',
          url:'Stepper.html'
        },
        {
          text:'Tab',
          url:'Tabs.html'
        }
        ]
      },
      {
        text: 'Belstar',
        icon: 'widgets',
        url: '/belstar',
        items: [
          {
            text:'Button',
            url:'button.html'
          }
        ]

      }
    ]
  }
}

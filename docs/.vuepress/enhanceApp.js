import Vuetify from 'vuetify';
import colors from 'vuetify/es5/util/colors';

export default ({
  Vue, // the version of Vue being used in the VuePress app
  options, // the options for the root Vue instance
  router, // the router instance for the app
  siteData // site metadata
}) => {
  Vue.use(Vuetify, {
    theme: {
      primary: colors.blue.darken1,
      secondary: colors.blue.lighten4,
      accent: colors.orange.darken1
    }
  });
}
